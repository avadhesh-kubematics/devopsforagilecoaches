# Provision a local VM for local component testing and local integration testing.

$provision_docker = <<-PROVISION_DOCKER
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io
sudo systemctl start docker
sudo usermod -aG docker $USER
PROVISION_DOCKER

$provision_compose = <<-PROVISION_COMPOSE
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
PROVISION_COMPOSE

$provision_java = <<-PROVISION_JAVA
wget https://download.java.net/java/GA/jdk14/076bab302c7b4508975440c56f6cc26a/36/GPL/openjdk-14_linux-x64_bin.tar.gz
tar xvf openjdk-14_linux-x64_bin.tar.gz
rm openjdk-14_linux-x64_bin.tar.gz
echo "export JAVA_HOME=/home/vagrant/jdk-14" >> ~/.bash_profile
export JAVA_HOME=/home/vagrant/jdk-14
echo "export PATH=${PATH}:${JAVA_HOME}/bin" >> ~/.bash_profile
export PATH=${PATH}:${JAVA_HOME}/bin
sudo yum install -y java
PROVISION_JAVA

$provision_maven = <<-PROVISION_MAVEN
wget http://apache.mirrors.hoobly.com/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
tar xvf apache-maven-3.6.3-bin.tar.gz
rm apache-maven-3.6.3-bin.tar.gz
echo "export PATH=${PATH}:/home/vagrant/apache-maven-3.6.3/bin" >> ~/.bash_profile
export PATH=${PATH}:/home/vagrant/apache-maven-3.6.3/bin
./apache-maven-3.6.3/bin/mvn -v
mkdir -p ~/.m2
wget https://gitlab.com/cliffbdf/devopsforagilecoaches/-/raw/bdd-component/toolchain-vm.xml
cp toolchain-vm.xml ~/.m2/toolchains.xml
PROVISION_MAVEN

$provision_java_utils = <<-PROVISION_JAVA_UTILS
git clone https://gitlab.com/cliffbdf/utilities-java.git
cd utilities-java
make install
cd
PROVISION_JAVA_UTILS

$provision_node = <<-PROVISION_NODE
wget https://nodejs.org/dist/v12.16.1/node-v12.16.1-linux-x64.tar.xz
tar xvf node-v12.16.1-linux-x64.tar.xz
rm node-v12.16.1-linux-x64.tar.xz
echo "export PATH=${PATH}:~/node-v12.16.1-linux-x64/bin" >> ~/.bash_profile
PROVISION_NODE

Vagrant.configure("2") do |config|
  config.vm.box = "bento/centos-7"
  config.vm.provision "shell", reboot: false, inline: $provision_docker
  config.vm.provision "shell", reboot: false, inline: $provision_compose
  config.vm.provision "shell", reboot: false, inline: $provision_java
  config.vm.provision "shell", reboot: false, inline: $provision_maven
  config.vm.provision "shell", reboot: false, inline: $provision_java_utils
  config.vm.provision "shell", reboot: true, inline: $provision_node
end
